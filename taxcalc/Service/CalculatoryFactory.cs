﻿using calculatetax.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace calculatetax.Service
{
    public class CalculatorFactory : ICalculatorFactory
    {
        private readonly IServiceProvider _provider;

        public CalculatorFactory(IServiceProvider provider)
        {
            _provider = provider;
        }

        public ICalculateTaxService GetCalculator(TaxItem item)
        {
            if(item.TaxCalculator == "TaxJar")
            {
                return (ICalculateTaxService)_provider.GetService(typeof(TaxJarTaxService));
            }

            // for this demo purpose I will just return the taxjar service as default
            return (ICalculateTaxService)_provider.GetService(typeof(TaxJarTaxService));
        }
    }
}
