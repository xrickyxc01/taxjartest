﻿using calculatetax.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace calculatetax.Service
{
    public interface ICalculatorFactory
    {
        public ICalculateTaxService GetCalculator(TaxItem item);
    }
}
