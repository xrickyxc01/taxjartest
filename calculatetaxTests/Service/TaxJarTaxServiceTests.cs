﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using calculatetax.Service;
using System;
using System.Collections.Generic;
using System.Text;
using calculatetax.Models;
using Moq;
using System.Text.Json;
using System.Threading.Tasks;
using System.IO;

namespace calculatetax.Service.Tests
{
    [TestClass()]
    public class TaxJarTaxServiceTests
    {
        public static TaxItem mockItem;
        public static ICalculateTaxService taxService;
        public static Mock<IServiceProvider> mockServiceProvider;
        public static Mock<TaxJarTaxService> mockCalcService;
        public static CalculatorFactory fac;

        [ClassInitialize]
        public static void TestFixtureSetup(TestContext context)
        {
            mockCalcService = new Mock<TaxJarTaxService>(mockServiceProvider);
            mockServiceProvider = new Mock<IServiceProvider>();
            mockItem = new TaxItem();
            CalculatorFactory fac = new CalculatorFactory(mockServiceProvider.Object);
            taxService = fac.GetCalculator(new TaxItem { TaxCalculator = "TaxJar" });

            mockItem.from_city = "La Jolla";
            mockItem.from_country = "US";
            mockItem.from_state = "CA";
            mockItem.from_zip = "92093";
            mockItem.from_street = "9500 Gilman Drive";

            mockItem.to_city = "Los Angeles";
            mockItem.to_country = "US";
            mockItem.to_state = "CA";
            mockItem.to_zip = "90002";
            mockItem.to_street = "1335 E 103rd St";

            // build tax mockItem with mock data for line items and nexus address
            var items = new List<LineItems>
            {
               new LineItems
               {
                   id = mockItem.id,
                   quantity = mockItem.quantity,
                   product_tax_code = mockItem.product_tax_code,
                   unit_price = mockItem.unit_price,
                   discount = mockItem.discount
               }
            };

            var nexus = new List<NexusAddresses>
            {
                new NexusAddresses
                {
                    id = "Main Location",
                    city = mockItem.to_city,
                    country = "US",
                    zip = mockItem.to_zip,
                    state = mockItem.to_state,
                    street = mockItem.to_street

                }
            };

            mockItem.line_items = items.ToArray();
            mockItem.nexus_addresses = nexus.ToArray();
        }

        [TestMethod()]
        public void TaxServiceIsNotNull()
        {
            Assert.IsNotNull(mockCalcService);
        }

        [TestMethod()]
        public async Task GetTaxWithLocationTest()
        {
            mockCalcService.Setup(x => x.GetTaxWithLocation(mockItem)).Returns(Task.FromResult("{\"status\":\"called\"}"));
            var result = await mockCalcService.Object.GetTaxWithLocation(mockItem);
            Assert.IsTrue(result == "{\"status\":\"called\"}");
        }

        [TestMethod()]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task GetTaxWithLocationTestException()
        {
            var result = await taxService.GetTaxWithLocation(null);
        }

        [TestMethod()]
        [ExpectedException(typeof(NullReferenceException))]
        public async void GetTaxesForOrderTestException()
        {
            var result = await mockCalcService.Object.GetTaxesForOrder(null);
        }
    }
}